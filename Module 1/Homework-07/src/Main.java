import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        System.out.println("1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.");
        try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
            reader.lines()
                    .map(x -> x.split("\\|"))
                    .filter( x -> x[2].equals("Черный") || x[3].equals("0"))
                    .forEach(x -> System.out.println(x[0]));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\n2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.");
        try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
            System.out.println(reader.lines()
                    .map(x -> x.split("\\|"))
                    .filter(x -> Integer.parseInt(x[4]) >= 700_000 && Integer.parseInt(x[4]) <= 800_000)
                    .map(x -> x[1])
                    .distinct()
                    .count());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\n3) Вывести цвет автомобиля с минимальной стоимостью.");
        try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
            System.out.println(reader.lines()
                    .map(x -> x.split("\\|"))
                    .min(Comparator.comparingInt(x -> Integer.parseInt(x[4])))
                    .stream().findFirst().map(x -> x[2]).orElse(""));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\n4) Среднюю стоимость Camry");
        try (BufferedReader reader = new BufferedReader(new FileReader("file.txt"))) {
            System.out.println(reader.lines()
                    .map(x -> x.split("\\|"))
                    .filter(x -> x[1].equals("Camry"))
                    .mapToInt(x -> Integer.parseInt(x[4]))
                    .average().orElse(0.0));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
