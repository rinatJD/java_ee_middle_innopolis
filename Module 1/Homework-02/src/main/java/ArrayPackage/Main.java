package ArrayPackage;

public class Main {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("rinat");
        list.add("rinat1");
        list.add("rinat2");
        list.add("rina3");
        list.add("rinat4");
        list.add("rinat5");

        System.out.println(list.get(0));
        System.out.println(list.get(-9));
        System.out.println(list.get(5));

        System.out.println("ITERATORS");

        Iterator<String> iterator1 = list.iterator();
        Iterator<String> iterator2 = list.iterator();

        while (iterator1.hasNext()) {
            System.out.println(iterator1.next());
        }

        iterator2.next();
        iterator2.next();
        iterator2.next();
        System.out.println(iterator2.next());
        iterator2.next();
        iterator2.next();
        iterator2.next();
        System.out.println(iterator2.next());

    }

}
