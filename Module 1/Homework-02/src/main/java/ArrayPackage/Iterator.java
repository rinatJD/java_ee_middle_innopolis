package ArrayPackage;

public interface Iterator<T> {

    boolean hasNext();
    T next();

}
