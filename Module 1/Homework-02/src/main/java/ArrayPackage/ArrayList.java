package ArrayPackage;

public class ArrayList<T> implements List<T> {

    private T[] array;
    public int size;

    public ArrayList() {
        array = (T[]) new Object[10];
        size  = 0;
    }

    @Override
    public void add(T element) {

        if (size == array.length) {

            T[] newArray = (T[]) new Object[size * 3 / 2];

            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
        array[size++] = element;
    }

    @Override
    public T get(int index) {
        if (index >=size || index < 0) {
            return null;
        }
        return array[index];
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }

    public class ArrayListIterator implements Iterator<T> {

        private int current = 0;

        @Override
        public boolean hasNext() {

            return array.length-1 >= current && array[current] != null;

        }

        @Override
        public T next() {

            if (hasNext()) {
//                T element = array[current];
//                if (current < array.length) {
//                    current++;
//                }
                return array[current++];
            }

            return null;

        }
    }

}
