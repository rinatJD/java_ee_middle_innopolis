
package ArrayPackage;
public interface List<T> {

    void add(T element);
    T get(int index);
    Iterator<T> iterator();

}
