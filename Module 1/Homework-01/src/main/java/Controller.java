
public class Controller {

    private TV tv;

    public Controller(TV tv) {
        this.tv = tv;
    }

    public void on(int i) {
        i--;
        System.out.println(tv.getChannel(i).getRandomProgram());
    }
}
