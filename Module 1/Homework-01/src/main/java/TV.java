
public class TV {

    private String model;
    private Channel[] channels = new Channel[MAX_CHANNEL];
    private static final int MAX_CHANNEL = 10;
    private int channelCount = 0;

    public TV(String model) {
        this.model = model;
    }

    public void addChannel(Channel channel) {
        this.channels[this.channelCount] = channel;
        this.channelCount++;
    }

    public Channel getChannel(int i) {
        if (i <0 || i > channelCount) {
            return null;
        }
        return channels[i];
    }

}
