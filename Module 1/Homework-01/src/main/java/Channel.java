import java.util.Random;

public class Channel {
    private String name;
    private Program[] programs = new Program[MAX_PROGRAM_COUNT];
    private static final int MAX_PROGRAM_COUNT = 10;
    private int programCount = 0;

    public Channel(String name) {
        this.name = name;
    }

    public void addProgram(Program program) {
        this.programs[this.programCount] = program;
        this.programCount++;
    }

    public Program getRandomProgram() {
        if (programCount == 0) {
            return null;
        }
        return programs[new Random().nextInt(programCount)];
    }

}
