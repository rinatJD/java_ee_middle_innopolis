
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryFileImpl implements UsersRepository {

    private final String fileName;
    private final IdGenerator idGenerator;

    public UsersRepositoryFileImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public void update(User user) {

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            List<String> list = new ArrayList<>();

            String userLine = reader.readLine();
            boolean fileModified = false;

            while (userLine != null) {

                String[] array = userLine.split("\\|");
                String email = array[1];

                String userAsLine;
                if (email.equals(user.getEmail())) {
                    userAsLine = array[0] + "|" + user.getEmail() + "|" + user.getPassword();
                    fileModified = true;
                } else {
                    userAsLine = array[0] + "|" + array[1] + "|" + array[2];
                }

                list.add(userAsLine);

                userLine = reader.readLine();

            }

            if (fileModified) {
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
                    for (String str : list) {
                        writer.write(str);
                        writer.newLine();
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            } else {
                throw new IllegalArgumentException();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public Optional<User> findByEmail(String email) {

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            String userLine = reader.readLine();

            while (userLine != null) {

                String[] array = userLine.split("\\|");
                String emailFile = array[1];

                if (emailFile.equals(email)) {
                    User user = new User(array[1], array[2]);
                    user.setId(Integer.parseInt(array[0]));
                    return Optional.ofNullable(user);
                }

                userLine = reader.readLine();

            }

            return Optional.empty();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<User> findAll() {

        List<User> list = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            String userLine = reader.readLine();

            while (userLine != null) {

                String[] array = userLine.split("\\|");

                User user = new User(array[1], array[2]);
                user.setId(Integer.parseInt(array[0]));

                list.add(user);

                userLine = reader.readLine();

            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return list;

    }

    @Override
    public void delete(User user) {

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            List<String> list = new ArrayList<>();

            String userLine = reader.readLine();
            boolean fileModified = false;

            while (userLine != null) {

                String[] array = userLine.split("\\|");

                String userAsLine;

                if (user.getId() == Integer.parseInt(array[0]) &&
                    user.getEmail().equals(array[1]) &&
                    user.getPassword().equals(array[2])) {
                    fileModified = true;
                } else {
                    userAsLine = array[0] + "|" + array[1] + "|" + array[2];
                    list.add(userAsLine);
                }

                userLine = reader.readLine();

            }

            if (fileModified) {
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
                    for (String str : list) {
                        writer.write(str);
                        writer.newLine();
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            } else {
                throw new IllegalArgumentException();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void save(User user) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
            user.setId(idGenerator.next());
            String userAsLine = user.getId() + "|" + user.getEmail() + "|" + user.getPassword();
            writer.write(userAsLine);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public int count() {

        int count = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            String userLine = reader.readLine();

            while (userLine != null) {

                count++;
                userLine = reader.readLine();

            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return count;

    }

    @Override
    public boolean existsByEmail(String email) {

        return Optional.ofNullable(findByEmail(email)).get().isPresent();

    }
}
