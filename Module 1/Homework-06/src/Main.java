
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        IdGenerator idGenerator = new IdGeneratorFileImpl("users_id.txt");
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt", idGenerator);
        UsersService usersService = new UsersService(usersRepository);

        Scanner scanner = new Scanner(System.in);

        List<User> list = usersRepository.findAll();

        for (User user : list) {
            System.out.println(user);
        }

        System.out.println(usersRepository.count());

//        int i = 2;
//
//        usersRepository.delete(list.get(i));

//        while (true) {
//            String email = scanner.nextLine();
//            String password = scanner.nextLine();
//
//            usersService.signUp(email, password);
//        }

    }
}
