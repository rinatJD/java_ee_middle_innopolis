import Exceptions.BadEmailException;
import Exceptions.BadPasswordException;
import Exceptions.UserNotFoundException;

public class Main {

    public static void main(String[] args) {

        UsersService usersService = new UsersServiceImpl();

        try {
            usersService.signUp("rinat@yandex.ru", "11x1111");
        } catch (BadPasswordException | BadEmailException e) {
            System.out.println(e.getMessage());
        }

        try {
            usersService.signIn("rinat@yandex.ru", "1x1111");
        } catch (UserNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }

}
