import Exceptions.BadEmailException;
import Exceptions.BadPasswordException;
import Exceptions.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UsersServiceImpl implements UsersService {

    List<User> list = new ArrayList<>();

    @Override
    public void signUp(String email, String password) throws BadEmailException, BadPasswordException {

        boolean numbersLetters = password.matches("[a-zA-Z0-9]*");
        boolean numbersOnly = password.matches("[0-9]*");
        boolean lettersOnly = password.matches("[a-zA-Z]*");

        if (!(numbersLetters && !numbersOnly && !lettersOnly) || password.length() < 7) {
            throw new BadPasswordException("Пароль должен состоять из букв и цифр и быть не короче 7 символов");
        }

        if (!email.contains("@")) {
            throw new BadEmailException("Неверный формат почты");
        }

        list.add(new User(email, password));

    }

    @Override
    public void signIn(String email, String password) throws UserNotFoundException {

        if (list.contains(new User(email, password))) {
            System.out.println("Пользователь успешно аутентифицирован");
        } else {
            throw new UserNotFoundException("Пользователь не найден");
        }

    }

}
