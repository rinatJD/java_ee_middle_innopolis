import Exceptions.BadEmailException;
import Exceptions.BadPasswordException;
import Exceptions.UserNotFoundException;

public interface UsersService {

        /**
         * Сохраняет пользователя с указанными данными в списке
         * @param email емейл пользователя (должен содержать символ @)
         * @param password пароль пользователя (должен состоять из букв и цифр, длина > 7)
         * @throws BadEmailException - если неверный формат почты
         * @throws BadPasswordException - если формат пароля неверный
         */
        void signUp(String email, String password) throws BadEmailException, BadPasswordException;

        /**
         * Аутентифицирует пользователя
         * @param email емейл пользователя
         * @param password пароль пользователя
         * @throws UserNotFoundException - если пользователь не был найден
         */
        void signIn(String email, String password) throws UserNotFoundException;
    }


