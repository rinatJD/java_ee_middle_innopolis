public class User {

    String email;
    String password;

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    public int hashCode() {
        return this.email.hashCode() + this.password.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        User user =(User) obj;
        return this.email.equals(user.email) &&
                this.password.equals(user.password);

    }
}
