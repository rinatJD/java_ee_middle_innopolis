import org.w3c.dom.ls.LSInput;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class Main {
    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        s.nextLine();

        ExecutorService service = Executors.newFixedThreadPool(15);

        AtomicLong size = new AtomicLong(0);

        try (Scanner scanner = new Scanner(new FileInputStream("files_list.txt"))) {

            AtomicInteger count = new AtomicInteger();

            while (scanner.hasNextLine()) {

                final String fileURL = scanner.nextLine();

                Future<Long> f = service.submit(() -> {

                    try (InputStream stream = new URL(fileURL).openStream()) {
                        count.getAndIncrement();
                        Files.copy(stream, Paths.get("download/" + count + ".txt"), StandardCopyOption.REPLACE_EXISTING);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return new File("download/" + count + ".txt").length();
                });

                size.addAndGet(f.get());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Общий размер файлов - " + size + " байт");

        service.shutdown();

    }
}
