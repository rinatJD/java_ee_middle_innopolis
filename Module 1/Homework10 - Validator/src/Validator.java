import java.lang.reflect.Field;
import java.util.Arrays;

public class Validator {

    public void validate(Object form) {

        Class formClass = form.getClass();
        Field[] fields = formClass.getDeclaredFields();

        for (Field field : fields) {
            checkNotEmpty(field, form);
            checkMin(field, form);
            checkMax(field, form);
            checkMinMaxLength(field, form);
        }

    }

    private void checkNotEmpty(Field field, Object form) {
        NotEmpty notEmpty = field.getAnnotation(NotEmpty.class);
        if (notEmpty != null) {
            try {
                field.setAccessible(true);
                String fieldText = (String) field.get(form);
                if (fieldText == null || fieldText.equals("")) {
                    throw new IllegalArgumentException();
                };
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        };
    }

    private void checkMin(Field field, Object form) {
        Min min = field.getAnnotation(Min.class);
        if (min != null) {
            try {
                field.setAccessible(true);
                if ((int) field.get(form) < min.value()) {
                    throw new IllegalArgumentException();
                };
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        };
    }

    private void checkMax(Field field, Object form) {
        Max max = field.getAnnotation(Max.class);
        if (max != null) {
            try {
                field.setAccessible(true);
                if ((int) field.get(form) > max.value()) {
                    throw new IllegalArgumentException();
                };
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        };
    }

    private void checkMinMaxLength(Field field, Object form) {
        MinMaxLength minMaxLength = field.getAnnotation(MinMaxLength.class);
        if (minMaxLength != null) {
            try {
                field.setAccessible(true);
                String fieldText = (String) field.get(form);
                int length = fieldText.length();
                if (length < minMaxLength.min() ||
                        length > minMaxLength.max()) {
                    throw new IllegalArgumentException();
                };
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        };
    }


}
