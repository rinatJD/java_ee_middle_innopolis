
public class Form {

    @NotEmpty
    private String name;
    @Min(18)
    @Max(55)
    private int Age;
    @MinMaxLength(min = 8, max = 20)
    private String description;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        Age = age;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
