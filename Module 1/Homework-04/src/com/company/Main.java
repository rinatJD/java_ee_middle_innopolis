package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();

        char[] array = line.toCharArray();

        Map<Character, Integer> map = new HashMap<>();

        for (char ch : array) {
            if (map.containsKey(ch)) {
                int count = map.get(ch);
                map.put(ch, ++count);
            } else {
                map.put(ch, 1);
            }
        }

        for (Character c : map.keySet()) {
            System.out.println(c + " - " + map.get(c));
        }

    }
}
