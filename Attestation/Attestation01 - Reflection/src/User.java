
public class User {
    private String name;
    private int age;
    private boolean isWorker;
    private String emeil;

    public User(String name, int age, boolean isWorker, String emeil) {
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
        this.emeil = emeil;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", emeil='" + emeil + '\'' +
                '}';
    }
}
