import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

        Processor processor = new Processor();
        Set<String> fieldsToCleanup = new HashSet<>();
        Set<String> fieldsToOutput = new HashSet<>();

        User user = new User("Rinat", 100, false, "sadvbsdfvb");
        System.out.println(user);
        fieldsToCleanup.add("name");
        fieldsToCleanup.add("age");
        fieldsToOutput.add("isWorker");
        fieldsToOutput.add("emeil");
        processor.cleanup(user, fieldsToCleanup, fieldsToOutput);
        fieldsToCleanup.clear();
        fieldsToOutput.clear();

        System.out.println("---------------------------");
        Map<String, Integer> map = new HashMap<>();
        map.put("ri", 6);
        map.put("ff", 256);
        map.put("f44", 100);
        System.out.println(map);
        fieldsToCleanup.add("ri");
//        fieldsToCleanup.add("ri4");
        fieldsToOutput.add("f44");
        processor.cleanup(map, fieldsToCleanup, fieldsToOutput);

        System.out.println("---------------------------");
        System.out.println(user);
        System.out.println(map);

    }

}
