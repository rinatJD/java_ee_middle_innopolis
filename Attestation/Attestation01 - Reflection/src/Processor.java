import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

public class Processor {
    void cleanup(Object object, Set<String> fieldsToCleanup, Set<String> fieldsToOutput) {

        boolean isMap = false;
        Class<?> classObject = object.getClass();

        Class<?>[] interfaces = classObject.getInterfaces();
        for (Class<?> currentInterface : interfaces) {
            if (currentInterface.getName().equals("java.util.Map")) {
                isMap =true;
                break;
            }
        }

        if (isMap) {
            Map<?, ?> map = (Map<?, ?>) object;

            for (String field : fieldsToCleanup) {
                if (!map.containsKey(field)) {
                    throw new IllegalArgumentException();
                }
            }
            for (String field : fieldsToOutput) {
                if (!map.containsKey(field)) {
                    throw new IllegalArgumentException();
                }
            }
            for (String field : fieldsToCleanup) {
                map.remove(field);
            }
            for (String field : fieldsToOutput) {
                System.out.println("Сконвертированное значение: " + map.get(field).toString());
            }
        } else {
            Field[] fields = classObject.getDeclaredFields();

            for (String field : fieldsToCleanup) {
                if (Arrays.stream(fields).noneMatch(x -> x.getName().equals(field))) {
                    throw new IllegalArgumentException();
                }
            }
            for (String field : fieldsToOutput) {
                if (Arrays.stream(fields).noneMatch(x -> x.getName().equals(field))) {
                    throw new IllegalArgumentException();
                }
            }

            for (Field field : fields) {

                try {

                    if (fieldsToCleanup.contains(field.getName())) {
                        field.setAccessible(true);
                        switch (field.getType().toString()) {
                            case "int", "double", "short", "long", "byte", "float" -> field.set(object, 0);
                            case "boolean" -> field.set(object, true);
                            case "char" -> field.set(object, '\u0000');
                            default -> field.set(object, null);
                        }
                    }

                    if (fieldsToOutput.contains(field.getName())) {
                        field.setAccessible(true);
                        switch (field.getType().toString()) {
                            case "int", "double", "short", "long", "byte", "float",
                                    "boolean", "char" -> System.out.println("Сконвертированное значение: " + String.valueOf(field.get(object)));
                            default -> System.out.println("Сконвертированное значение: " + field.get(object).toString());
                        }
                    }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
