import java.io.FileInputStream;
import java.util.Properties;

public class Config {

    private final String fileName;
    private final String DB_USER;
    private final String DB_PASSWORD;
    private final String DB_URL;

    public Config(String fileName) {
        this.fileName = fileName;

        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(fileName));
            this.DB_USER = properties.getProperty("DB_USER");
            this.DB_PASSWORD = properties.getProperty("DB_PASSWORD");
            this.DB_URL = properties.getProperty("DB_URL");
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }


    }

    public String getDB_USER() {
        return DB_USER;
    }

    public String getDB_PASSWORD() {
        return DB_PASSWORD;
    }

    public String getDB_URL() {
        return DB_URL;
    }
}
