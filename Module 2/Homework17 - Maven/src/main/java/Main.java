import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repositories.ProductRepository;
import repositories.ProductRepositoryImpl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"--hikari-pool-size"})
    private static int poolSize;

    private static final int MAX_CLIENTS_COUNT = 300;

    public static void main(String[] args) {

        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        Config config = new Config("resources/connection.properties");

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername(config.getDB_USER());
        hikariConfig.setPassword(config.getDB_PASSWORD());
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setJdbcUrl(config.getDB_URL());
        hikariConfig.setMaximumPoolSize(poolSize);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        ProductRepository productRepository = new ProductRepositoryImpl(dataSource);

        ExecutorService service = Executors.newFixedThreadPool(MAX_CLIENTS_COUNT);

        for (int clientNumber = 0; clientNumber < MAX_CLIENTS_COUNT; clientNumber++) {
            service.submit(() -> {
                for (long i = 0; i < 10; i++) {
                    try {
                        System.out.println(productRepository.findById(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        service.shutdown();

    }

}

