import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repositories.ProductRepository;
import repositories.ProductRepositoryImpl;

import org.apache.log4j.Logger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Parameters(separators = "=")
public class Main {

    @Parameter(names = {"--hikari-pool-size"})
    private static int poolSize;

    private static final int MAX_CLIENTS_COUNT = 3;

    private static final Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        Main main = new Main();
        JCommander.newBuilder()
                .addObject(main)
                .build()
                .parse(args);

        Config config = new Config("src\\main\\resources\\connection.properties");

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername(config.getDB_USER());
        hikariConfig.setPassword(config.getDB_PASSWORD());
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setJdbcUrl(config.getDB_URL());
        hikariConfig.setMaximumPoolSize(poolSize == 0 ? 3 : poolSize);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        ProductRepository productRepository = new ProductRepositoryImpl(dataSource);

        ExecutorService service = Executors.newFixedThreadPool(MAX_CLIENTS_COUNT);

        logger.warn("Привет");

        for (int clientNumber = 0; clientNumber < MAX_CLIENTS_COUNT; clientNumber++) {
            service.submit(() -> {
                logger.debug("Here");
                for (long i = 0; i < 10; i++) {
                    try {
                        System.out.println(productRepository.findById(i));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        service.shutdown();

    }

}

