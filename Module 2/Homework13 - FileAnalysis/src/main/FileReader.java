package main;

import main.strategy.Reader;

public class FileReader {

    private Reader reader;
    private final String fileName;

    public FileReader(String fileName) {
        this.fileName = fileName;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public void read() {
        reader.read(this.fileName);
    }

}
