package main.chain;

public class MileageFileHandler extends BaseFileHandler {
    @Override
    public void handle(String fileName) {
        writeColumn(fileName, "mileages.txt", 3);
        nextHandler(fileName);
    }
}
