package main.chain;

public class PriceFileHandler extends BaseFileHandler {
    @Override
    public void handle(String fileName) {
        writeColumn(fileName, "prices.txt", 4);
        nextHandler(fileName);
    }
}
