package main.chain;

public class ModelFileHandler extends BaseFileHandler {
    @Override
    public void handle(String fileName) {
        writeColumn(fileName, "models.txt", 1);
        nextHandler(fileName);
    }
}
