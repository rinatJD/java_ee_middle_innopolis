package main.chain;

public class NumberFileHandler extends BaseFileHandler {
    @Override
    public void handle(String fileName) {
        writeColumn(fileName, "numbers.txt", 0);
        nextHandler(fileName);
    }
}
