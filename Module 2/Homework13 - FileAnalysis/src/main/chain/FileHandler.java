package main.chain;

public interface FileHandler {
    void handle(String fileName);
    void setNextHandler(FileHandler handler);
}
