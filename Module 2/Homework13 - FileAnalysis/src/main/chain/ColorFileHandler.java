package main.chain;

public class ColorFileHandler extends BaseFileHandler {
    @Override
    public void handle(String fileName) {
        writeColumn(fileName, "colors.txt", 2);
        nextHandler(fileName);
    }
}
