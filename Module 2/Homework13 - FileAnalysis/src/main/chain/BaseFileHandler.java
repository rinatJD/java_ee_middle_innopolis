package main.chain;

import java.io.*;

public abstract class BaseFileHandler implements FileHandler {
    protected FileHandler next;

     @Override
    public void setNextHandler(FileHandler handler) {
        this.next = handler;
    }

    public void nextHandler(String fileName) {
        if (next != null) {
            next.handle(fileName);
        }
    }

    public void writeColumn(String fileNameSource, String fileNameReceiver, int columnNumber) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileNameSource));
             BufferedWriter writer = new BufferedWriter(new FileWriter(fileNameReceiver))) {

            String line = reader.readLine();

            while (line != null) {

                String[] array = line.split("\\|");

                writer.write(array[columnNumber]);
                writer.newLine();

                line = reader.readLine();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
