package main.strategy;

import main.observer.FileObserver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class StreamReader implements Reader {
    List<FileObserver> observerList = new ArrayList<>();

    public void addObserver(FileObserver observer) {
        observerList.add(observer);
    }
    @Override
    public void read(String fileName) {
        try {

            Files.lines(Paths.get(fileName))
                    .forEach(System.out::println);

            for (FileObserver observer : observerList) {
                Files.lines(Paths.get(fileName))
                        .forEach(observer::observe);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
