package main.strategy;

import main.observer.FileObserver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LineReader implements Reader {

    List<FileObserver> observerList = new ArrayList<>();

    public void addObserver(FileObserver observer) {
        observerList.add(observer);
    }

    @Override
    public void read(String fileName) {

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){

            String line = reader.readLine();

            while (line != null) {
                System.out.println(line);
                for (FileObserver observer : observerList) {
                    observer.observe(line);
                }
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
