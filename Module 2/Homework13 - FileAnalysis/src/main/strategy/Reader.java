package main.strategy;

public interface Reader {
    void read(String fileName);
}
