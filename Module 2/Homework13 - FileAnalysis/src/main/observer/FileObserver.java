package main.observer;

public interface FileObserver {
    void observe(String line);
}
