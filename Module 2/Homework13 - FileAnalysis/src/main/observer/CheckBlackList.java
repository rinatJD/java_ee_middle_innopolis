package main.observer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class CheckBlackList implements FileObserver {
    @Override
    public void observe(String line) {
        String number = line.split("\\|")[0];
        if (!number.matches("[a-zA-Z][0-9]{3}[a-zA-Z]{2}")) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("blacklist.txt", true))) {
                writer.write(number);
                writer.newLine();
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
