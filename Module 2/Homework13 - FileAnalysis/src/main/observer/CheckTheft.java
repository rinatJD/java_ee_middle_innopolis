package main.observer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CheckTheft implements FileObserver {
    @Override
    public void observe(String line) {

        try (BufferedReader reader = new BufferedReader(new FileReader("stolenAuto.txt"))) {
            String str = reader.readLine();
            while (str != null) {
                String number = line.split("\\|")[0];
                if (number.equals(str)) {
                    System.out.println("Автомобиль с номером " + line + " числится угнанным");
                    break;
                }
                str = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
