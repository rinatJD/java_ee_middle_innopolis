import main.FileReader;
import main.chain.*;
import main.observer.CheckBlackList;
import main.observer.CheckTheft;
import main.observer.FileObserver;
import main.strategy.*;


public class Main {

    public static void main(String[] args) {

        final String FILE_NAME = "file.txt";

        // паттерн Цепочка обязанностей
        NumberFileHandler numberFileHandler = new NumberFileHandler();
        ModelFileHandler modelFileHandler = new ModelFileHandler();
        ColorFileHandler colorFileHandler = new ColorFileHandler();
        MileageFileHandler mileageFileHandler = new MileageFileHandler();
        PriceFileHandler priceFileHandler = new PriceFileHandler();

        numberFileHandler.setNextHandler(modelFileHandler);
        modelFileHandler.setNextHandler(colorFileHandler);
        colorFileHandler.setNextHandler(mileageFileHandler);
        mileageFileHandler.setNextHandler(priceFileHandler);

        numberFileHandler.handle(FILE_NAME);

        // паттерны Стратегия и Наблюдатель

        FileObserver checkTheft = new CheckTheft();
        FileObserver checkBlackList = new CheckBlackList();

        FileReader reader = new FileReader(FILE_NAME);

        System.out.println("==============Stream API==============");
        StreamReader streamReader = new StreamReader();
        streamReader.addObserver(checkTheft);
        streamReader.addObserver(checkBlackList);
        reader.setReader(streamReader);
        reader.read();

        System.out.println("==============reader.readLine==============");
        LineReader lineReader = new LineReader();
        lineReader.addObserver(checkTheft);
        lineReader.addObserver(checkBlackList);
        reader.setReader(lineReader);
        reader.read();

    }
}
