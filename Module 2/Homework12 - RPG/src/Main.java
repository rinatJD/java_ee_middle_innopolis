import Armory.*;

public class Main {

    public static void main(String[] args) {

        Weapon weapon = Weapon.builder()
                .damage(56)
                .weight(2.30)
                .description("Меч двуручный")
                .type(Weapon.Type.SWORD)
                .build();

        Weapon weapon1 = weapon.clone();
        weapon1.setType(Weapon.Type.BOW);
        weapon1.setDescription("Лук охотничий");

        System.out.println(weapon);
        System.out.println(weapon1);

        IronWeapon ironWeapon = new IronWeapon();
        Sword sword = ironWeapon.createSword();
        System.out.println(sword.getDescription());

        StoneWeapon stoneWeapon = new StoneWeapon();
        Bow bow = stoneWeapon.createBow();
        System.out.println(bow.getDescription());

    }



}
