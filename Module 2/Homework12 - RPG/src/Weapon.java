public class Weapon implements Cloneable {

    @Override
    public Weapon clone() {
        return Weapon.builder()
                .damage(this.damage)
                .type(this.type)
                .description(this.description)
                .weight(this.weight)
                .build();
    }

    public enum Type {
        SWORD, BOW, SPEAR
    }

    private double damage;
    private double weight;
    private String description;
    private Type type;

    private Weapon(Builder builder) {
        this.damage = builder.damage;
        this.weight = builder.weight;
        this.description = builder.description;
        this.type = builder.type;
    }

    public static class Builder {

        private double damage;
        private double weight;
        private String description;
        private Type type;

        public Builder damage(double damage) {
            this.damage = damage;
            return this;
        }

        public Builder weight(double weight) {
            this.weight = weight;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        public Weapon build() {
            return new Weapon(this);
        }

    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "damage=" + damage +
                ", weight=" + weight +
                ", description='" + description + '\'' +
                ", type=" + type +
                '}';
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
