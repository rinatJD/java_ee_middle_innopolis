package Armory;

public interface Sword {
    String getDescription();
}
