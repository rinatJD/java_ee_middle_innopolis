package Armory;

public interface Armory {
    Sword createSword();
    Bow createBow();
    Spear createSpear();
}
