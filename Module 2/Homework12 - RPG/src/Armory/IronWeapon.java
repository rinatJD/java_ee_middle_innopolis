package Armory;

public class IronWeapon implements Armory {

    public static class ironSword implements Sword{
        @Override
        public String getDescription() {
            return "Железный меч";
        }
    }

    public static class ironBow implements Bow {
        @Override
        public String getDescription() {
            return "Железный лук";
        }
    }

    public static class ironSpear implements Spear {
        @Override
        public String getDescription() {
            return "Железное копьё";
        }
    }

    @Override
    public Sword createSword() {
        return new ironSword();
    }

    @Override
    public Bow createBow() {
        return new ironBow();
    }

    @Override
    public Spear createSpear() {
        return new ironSpear();
    }
}
