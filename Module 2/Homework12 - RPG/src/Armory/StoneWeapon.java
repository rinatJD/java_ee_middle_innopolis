package Armory;

public class StoneWeapon implements Armory {

    public static class stoneSword implements Sword{
        @Override
        public String getDescription() {
            return "Каменный меч";
        }
    }

    public static class stoneBow implements Bow {
        @Override
        public String getDescription() {
            return "Каменный лук";
        }
    }

    public static class stoneSpear implements Spear {
        @Override
        public String getDescription() {
            return "Каменное копьё";
        }
    }

    @Override
    public Sword createSword() {
        return new stoneSword();
    }

    @Override
    public Bow createBow() {
        return new stoneBow();
    }

    @Override
    public Spear createSpear() {
        return new stoneSpear();
    }
}
