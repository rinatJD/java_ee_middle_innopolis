package Armory;

public interface Bow {
    String getDescription();
}
