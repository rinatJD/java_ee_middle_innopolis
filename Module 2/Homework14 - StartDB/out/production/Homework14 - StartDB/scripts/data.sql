insert into product (name) values ('Холодильник');
insert into product (name) values ('Телевизор');
insert into product (name) values ('Микроволновка');
insert into product (name) values ('Стиралка');

insert into buyer (name) values ('ООО Рога и копыта');
insert into buyer (name) values ('Иванов Иван');
insert into buyer (name) values ('Розничный покупатель');

insert into orders (product_id, buyer_id, count, price) values (1, 2, 5, 50000);
insert into orders (product_id, buyer_id, count, price) values (2, 2, 7, 75000);
insert into orders (product_id, buyer_id, count, price) values (4, 2, 10, 20000);
insert into orders (product_id, buyer_id, count, price) values (2, 1, 10, 27000);
insert into orders (product_id, buyer_id, count, price) values (4, 1, 10, 90000);

insert into booking_of_goods (product_id, count) values (1, 5);
insert into booking_of_goods (product_id, count) values (2, 7);
insert into booking_of_goods (product_id, count) values (4, 10);

insert into booking_of_order (orders_id) values (1);

insert into receipt (product_id, count, sum) values (1, 5, 250000);
insert into receipt (product_id, count, sum) values (2, 7, 4564654);


