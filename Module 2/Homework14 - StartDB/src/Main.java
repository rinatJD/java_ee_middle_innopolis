import java.sql.*;

public class Main {

    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "0990";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/HW-14_javaEE";

    public static void main(String[] args) {

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        // выводит сообщение типа "Иванов Иван заказал 5 шт. продукции: Холодильник"
        String qeury = "select *, b.name as buyer from orders as o inner join product as p ON o.product_id = p.id inner join buyer b on o.buyer_id = b.id";
        // выводит номера чеков, сумма которые больше 100
        String query1 = "select id from receipt where sum > ?";

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(qeury);
             PreparedStatement preparedStatement1 = connection.prepareStatement(query1)) {

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                System.out.println(resultSet.getString("buyer") + " заказал " +
                        resultSet.getInt("count") + " шт. продукции: " +
                        resultSet.getString("name") );
            }

            preparedStatement1.setInt(1,100);
            ResultSet resultSet1 = preparedStatement1.executeQuery();

            System.out.println();

            if (resultSet1.next()) {
                System.out.println("Чеки свыше 100 у.е.:");
                System.out.println("Чек номер " + resultSet1.getInt("id"));

                while (resultSet1.next()) {
                    System.out.println("Чек номер " + resultSet1.getInt("id"));
                }
            }

        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }
}

