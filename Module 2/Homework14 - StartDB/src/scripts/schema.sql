create table product
(
    id         serial primary key,
    name varchar(200)
);

create table buyer
(
    id         serial primary key,
    name varchar(200)
);

create table orders
(
    id serial primary key,
    product_id integer,
    buyer_id  integer,
    count integer,
    price real,
    foreign key (product_id) references product (id),
    foreign key (buyer_id) references buyer (id)
);

create table booking_of_goods
(
    id serial primary key,
    product_id integer,
    count integer,
    foreign key (product_id) references product (id)
);

create table booking_of_order
(
    id serial primary key,
    orders_id integer,
    foreign key (orders_id) references orders (id)
);

create table receipt
(
    id serial primary key,
    product_id integer,
    count integer,
    sum integer,
    foreign key (product_id) references product (id)
)

