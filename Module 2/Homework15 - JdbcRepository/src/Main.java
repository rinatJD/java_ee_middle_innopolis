import models.Product;
import repositories.ProductRepository;
import repositories.ProductRepositoryImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {


    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "0990";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/HW-14_javaEE";

    public static void main(String[] args) {

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)) {

            ProductRepository productRepository = new ProductRepositoryImpl(connection);

            System.out.println("Поиск по id");
            Optional<Product> productOptional = productRepository.findById(1L);
            if (productOptional.isPresent()) {
                System.out.println(productOptional.get());
            } else {
                System.out.println("Товар с таким id не существует");
            }

            System.out.println("Вывести список товаров");
            productRepository.findAll(0, 100).forEach(System.out::println);

            System.out.println("Добавление нового товара");
            Product newProduct = new Product("Кимоно");
            productRepository.save(newProduct);

            System.out.println("Изменение существующего товара");
            Product productUpdate = new Product(1L, "Холодильник Stinol");
            productRepository.update(productUpdate);

            System.out.println("Удаление по id");
            productRepository.deleteById(15L);

            System.out.println("Удаление товара");
            Product productDelete = new Product(19L, "Кимоно");
            productRepository.delete(productDelete);

        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }

    }
}
